<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
  <link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
  <div class="nav"><br>
  <?php
    include_once ("database.php");
    connectOnDatabase();
    include_once ("functions.php");
    include_once ("current_user_functions.php");
    $userId = getLoggedUserId();
    if (!empty($userId)) {
  ?>
    <p>
      <a href="logout.php">Odjavi se</a>
    </p>    
  <?php
    } else {
      echo " ";
    } 
  ?>

  <div class="nav_container">
    <ul>
      <?php 
        $userId = getLoggedUserId();
        if (empty($userId)) {
      ?>
        <li><a href="../php_scripts/login_form.php">Prijavi se</a></li>
        <li><a href="../php_scripts/register_form.php">Registriraj se</a></li>
        <li><a href="../php_scripts/displaying_association.php">Popis udruga</a></li>
        <li><a href="../php_scripts/o_autoru.php">Podaci o autoru</a></li>  
      <?php 
        } else {
          echo " ";
        }
      ?>

      <?php
        $userType = getUserType();
        $userId = getLoggedUserId();
        if(!empty($userId) && $userType == 2) {
      ?>
        <li><a href="./index.php">Početna</a></li>
        <li><a href="../php_scripts/displaying_association.php">Popis udruga</a></li>
        <li><a href="../php_scripts/activity.php">Popis aktivnosti</a></li>
        <li><a href="../php_scripts/o_autoru.php">Podaci o autoru</a></li>
      <?php
        } else {
          echo " ";
        }
      ?>

      <?php
        $userType = getUserType();
        $userId = getLoggedUserId();
        if(!empty($userId) && $userType == 1) {
      ?>
        <li><a href="./index.php">Početna</a></li>
        <li><a href="../php_scripts/displaying_association.php">Popis udruga</a></li>
        <li><a href="../php_scripts/activity.php">Popis aktivnosti</a></li>
        <li><a href="../php_scripts/o_autoru.php">Podaci o autoru</a></li>
      <?php
        } else {
          echo " ";
        }
      ?>

      <?php
        $userType = getUserType();
        $userId = getLoggedUserId();
        if(!empty($userId) && $userType == 0) {
      ?>
        <li><a href="./index.php">Početna</a></li>
        <li><a href="../php_scripts/displaying_association.php">Popis udruga</a></li>
        <li><a href="../php_scripts/displaying_users.php">Popis korisnika</a></li>
        <li><a href="../php_scripts/activity.php">Popis aktivnosti</a></li>
        <li><a href="../php_scripts/o_autoru.php">Podaci o autoru</a></li>
      <?php
        } else {
          echo " ";
        }
      ?>
    </ul>
  </div>

  <?php
  	include_once ("database.php");
  	connectOnDatabase();
  	include_once ("functions.php");
    include_once ("current_user_functions.php");

  	$userId = getLoggedUserId();

		$sql = "SELECT * FROM korisnik k, tip_korisnika t WHERE k.korisnik_id='$userId' AND t.tip_id= k.tip_id ";
    $query_user = executeQuery($sql); 

    $user = mysql_fetch_array($query_user); 
  ?>

  <div class="loggedUser">
    <?= $user["ime"]?>-<?=$user["naziv"] ?>
  </div>
  </div>
</body>
