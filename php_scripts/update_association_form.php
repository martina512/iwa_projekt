<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");
		include_once ("functions.php");
		include_once ("database.php");
		include_once ("current_user_functions.php");
		connectOnDatabase();

		$userId = getLoggedUserId();

		if (empty($userId) || $userType == 1 || $userType == 2) {
			header("Location: redirect_page.php");
		}

		$association_id = $_GET['udruga_id'];

		$sql = "SELECT * FROM udruga WHERE udruga_id = '$association_id'";
		$result = executeQuery($sql);
		$association = mysql_fetch_array($result);
	?>
	<div class="form">
		<form action="update_association.php" method="POST" accept-charset="utf-8">
			<p>
				<input type="hidden" name="id" value="<?= $association['udruga_id'] ?>" placeholder=""></input>
			</p>

			<p>
				<label>Moderator id:</label>
				<select name="moderator_id" class="select">
					<?php
						$sql = "SELECT * FROM korisnik WHERE tip_id=1 ";
						$queryModerator = executeQuery($sql);
						 while ($moderator = mysql_fetch_array($queryModerator)) {
					?>
						<option value="<?= $moderator['korisnik_id'] ?>"><?= $moderator['korisnicko_ime'] ?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				<label>Naziv udruge:</label>
				<input type="name" name="name" value="<?= $association['naziv'] ?>" placeholder=""></input>
			</p>
			<p>
				<label>Opis udruge:</label>
				<textarea rows="6" cols="50" name="content"><?= $association['opis'] ?></textarea>
			</p>
			<p>
				<input type="submit" name="update_association" class="btn" value="Spremi promjene udruge"></input>
			</p>
		</form>
	</div>
</body>
</html>

