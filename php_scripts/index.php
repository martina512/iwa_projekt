<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
	</head>
	<body>
		<?php 
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");

			connectOnDatabase();

			$userId = getLoggedUserId();

			if (empty($userId)) {
				header("Location: login_form.php");
			} 

			$userId = getUserName();
			$userType = getUserType();

			if (!empty($userId) && $userId != false) {
		?>
			<div>
				<span>Dobrodošli: <?= $userId ?></span>	
			</div>
		<?php
			} else	{
		?>
				<div>Nisi prijavljen</div>
		<?php } ?>
	</body>
</html>
