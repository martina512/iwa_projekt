<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");
			connectOnDatabase();

			$userId = getLoggedUserId();
			$userType = getUserType();

			$udruga_id = $_GET['udruga_id'];

			$sql = "SELECT * FROM udruga WHERE udruga_id ='$udruga_id'";
			$query_association = executeQuery($sql); 

			$association = mysql_fetch_array($query_association);


			$sqlModerator = "SELECT * FROM korisnik k, udruga u WHERE k.korisnik_id = u.moderator_id AND u.moderator_id='$userId' and u.udruga_id='$udruga_id'";
			$query_moderator = executeQuery($sqlModerator); 

			$moderator = mysql_fetch_array($query_moderator);

			$isAssociationModerator = false;
			if ($moderator) {
				$isAssociationModerator = true;
			}
		?>	

		<div>
			<table class="tbl_association">
				<p>Detalji udruge</p>
				<tr>
					<th>ID udruge:</th>
					<td><?= $association["udruga_id"] ?></td>
				</tr>
				<tr>
					<th>Naziv udruge:</th>
					<td><?= $association["naziv"] ?></td>
				</tr>
				<tr>
					<th>Opis udruge:</th>
					<td><?= $association["opis"] ?></td>
				</tr>
				<tr>
				<?php if(!empty($userId) && ($userType == 0 || $isAssociationModerator)) {
				?>
					<th>Kreiraj novu aktivnost udruge:</th>
					<td><a href="./created_activity_form.php?udruga_id=<?= $association['udruga_id'] ?>">Kreiraj novu aktivnost</a></td>
				<?php } ?>
				</tr>

				<tr>
				<?php if(!empty($userId) && $userType == 0) {
				?>
					<th>Ažuriraj udrugu:</th>
					<td><a href="./update_association_form.php?udruga_id=<?= $association['udruga_id'] ?>">Ažuriraj udrugu</a></td>
				<?php } ?>
				</tr>
			</table>
		</div>

		<?php 
			
			$udruga_id = $_GET['udruga_id'];

			$sql = "SELECT * FROM aktivnost WHERE udruga_id ='$udruga_id'";
			$query_activity = executeQuery($sql); 

		?>
			<p>Aktivnosti udruge:</p>
				<table class="tbl">
					<tr>
						<th>Naziv aktivnosti</th>
						<?php if(!empty($userId) && $userType == 0) {
						?>
							<th>Ažuriraj aktivnost</th>			
						<?php }	?>
					</tr>
					<tr>
						<?php while ($activity = mysql_fetch_array($query_activity)) { ?>
						<td><?= $activity["naziv"] ?></td>
						<?php if(!empty($userId) && $userType == 0) {
						?>
							<td><a href="./update_activity_form.php?aktivnost_id=<?= $activity['aktivnost_id'] ?>">Ažuriraj aktivnost</a></td>
						<?php }	?>
					</tr>
					<?php }	?>
				</table>	

	</body>
</html>
