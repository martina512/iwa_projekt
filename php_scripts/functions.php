<?php
	
	function executeQuery($sql) {
		$query = mysql_query($sql);
		return $query;				
	}

	function getDateFromDateTime($dateTime) {
		$dateArray = explode(" ", $dateTime);

		$date = explode(".", $dateArray[0]);

		$day = $date[0];
		$month = $date[1];
		$year = $date[2];

		if (strlen($day) == 1) {
			$day = '0'.$day;
		}

		if (strlen($month) == 1) {
			$month = '0'.$month;
		}

		return $year."-".$month."-".$day;
	}

	function getTimeFromDateTime($dateTime) {
		$dateArray = explode(" ", $dateTime);
		return $dateArray[1];
	}

	function getFormattedDate($date) {
		$newdate = explode(".", $date);

		$year = $newdate[2];
		$month = $newdate[1];
		$day = $newdate[0];

		if (strlen($day) == 1) {
			$day = '0'.$day;
		}

		if (strlen($month) == 1) {
			$month = '0'.$month;
		}

		return $year."-".$month."-".$day;
	}


	function formatDate() {
		$t = time();
		$p = (date("Y-m-d", $t));
		return $p;
	}

	function formatTime() {
		$t = time();
		$p = (date("H:i:s", $t));
		return $p;
	}

	function getHumanReadableDate($date) {
		$newdate = explode("-", $date);
		$year = $newdate[0];
		$month = $newdate[1];
		$day = $newdate[2];

		if (strlen($day) == 1) {
			$day = '0'.$day;
		}

		if (strlen($month) == 1) {
			$month = '0'.$month;
		}

		return $day.".".$month.".".$year;
	}
?>