<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");
		include_once ("current_user_functions.php");

		$userType = getUserType();
		$userId = getLoggedUserId();
		
		if (empty($userId) || $userType == 1 || $userType == 2) {
			header("Location: redirect_page.php");
		}
	?>

	<div class="form">
		<form action="created_user.php" method="POST" accept-charset="utf-8">
			<p>
				<label>Tip korisnika:</label>
				<?php
					include_once ("database.php");
					include_once ("functions.php");
					include_once ("current_user_functions.php");

					connectOnDatabase();

					$sql = "SELECT * FROM tip_korisnika";
					$query = executeQuery($sql);
				?>

				<select name="userType" required="">
					<?php while ($row = mysql_fetch_array($query)) { ?>
						<option value="<?= $row['tip_id'] ?>"><?= $row['naziv'] ?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				<label>Ime:</label>
				<input type="name" name="name" value="" placeholder="Upiši ime" required=""></input>
			</p>
			<p>
				<label>Prezime:</label>
				<input type="surname" name="surname" value="" placeholder="Upiši prezime" required=""></input>
			</p>
			<p>
				<label>Email:</label>
				<input type="email" name="email" value="" placeholder="Upiši email" required=""></input>
			</p>
			<p>
				<label>Korisničko ime:</label>
				<input type="text" name="username" value="" placeholder="Upiši korisničko ime" required=""></input>
			</p>
			<p>
				<label>Lozinka:</label>
				<input type="password" name="password" value="" placeholder="Upiši lozinku" required=""></input>
			</p>
			<p>
				<input type="submit" name="created_user" class="btn" value="Kreiraj novog korisnika"></input>
			</p>
		</form>
	</div>
</body>
</html>