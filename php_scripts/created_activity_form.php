<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");

		$association_id = $_GET['udruga_id'];
	?>
	<div class="form">
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("current_user_functions.php");

			connectOnDatabase();

			$userId = getLoggedUserId();
			$userType = getUserType();

			if (empty($userId) || $userType == 2 ) {
				header("Location: redirect_page.php");
			} 

			$sql = "SELECT * FROM korisnik";
			$queryUsers = executeQuery($sql);
		?>

		<form action="created_activity.php" method="POST" accept-charset="utf-8">
			<input type="hidden" value="<?= $association_id ?>" name="udrugaId"></input>
			<p>
				<label>Datum održavanja aktivnosti:(upiši format: dd.mm.gggg)</label>
				<input type="text" name="start_date" value="" placeholder="" required=""></input>		
			</p>
			<p>
				<label>Vrijeme održavanja aktivnosti:(upiši format: hh:mm:ss)</label>
				<input type="text" step="1" name="start_time" value="" placeholder="" required=""></input>
			</p>
			<p>
				<label>Naziv aktivnosti:</label>
				<input type="name" name="name" value="" placeholder="Upiši naziv aktivnosti" required=""></input>
			</p>
			<p>
				<label>Opis aktivnosti:</label>
				<textarea rows="5" cols="50" name="content" placeholder="Upiši opis aktivnosti"></textarea>
			</p>
			<p>
				<label>Sudionici: (za odabir više sudionika drži tipku CRTL)</label>
				<select name="participants[]" multiple size="20" class="select" required="">
					<?php while ($user = mysql_fetch_array($queryUsers)) { ?>
						<option value="<?= $user['korisnik_id'] ?>"><?= $user['korisnicko_ime'] ?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				<input type="submit" name="create_activity" class="btn" value="Kreiraj novu aktivnost"></input>
			</p>
		</form>
	</div>
</body>
</html>