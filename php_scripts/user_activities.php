<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");
			connectOnDatabase();

			$userId = getLoggedUserId();

			if (empty($userId)) {
				header("Location: redirect_page.php");
			}

	
			$aktivnost_id = $_GET['aktivnost_id'];

			$sql = "SELECT * FROM korisnik k, sudionik s WHERE 
			k.korisnik_id = s.korisnik_id AND s.aktivnost_id = '$aktivnost_id' ";
			 
			$query_user = executeQuery($sql); 
		?>

		<table class="tbl_activity">
			<tr>
				<th>Popis svih sudionika aktivnosti <?= $aktivnost_id ?>:</th>
			</tr>
			<?php while ($users = mysql_fetch_array($query_user)) { ?>
				<tr>
					<td><?= $users["korisnicko_ime"] ?></td>
				</tr>
			<?php } ?>
		</table>
				
	</body>
</html>