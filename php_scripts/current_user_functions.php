<?php
	session_start();
	$current_user_id = "currentUserId";
	$current_user_type = "currentUserType";
	$current_user_name = "currentUserName";

	function getLoggedUserId() {
		global $current_user_id;

		if (isset($_SESSION[$current_user_id])) {
			$userId = $_SESSION[$current_user_id];
		 	return $userId;
		} else {
			return false;
		}
	}

	function setCurrentUser($user) {
		global $current_user_id;
		global $current_user_type;
		global $current_user_name;
		$_SESSION[$current_user_id] = $user['korisnik_id'];
		$_SESSION[$current_user_type] = $user['tip_id'];
		$_SESSION[$current_user_name] = $user['korisnicko_ime'];
	}

	function getUserType() {
		global $current_user_type;
		if(isset($_SESSION[$current_user_type])) {
			$userType = $_SESSION[$current_user_type];
			return $userType;
		} else {
			return false;
		}
	}


	function getUserName() {
		global $current_user_name;
		if(isset($_SESSION[$current_user_name])) {
			$userName = $_SESSION[$current_user_name];
			return $userName;
		} else {
			return false;
		}
	}

	function logout() {
		global $current_user_id;
		$_SESSION[$current_user_id] = '';
		header("Location: login_form.php");
	}
?>


	