<?php
	include_once ("database.php");
	include_once ("functions.php");
	include_once ("navigation.php");
	include_once ("current_user_functions.php");
	
	connectOnDatabase();

	$userId = getLoggedUserId();
	
	if (empty($userId) || $userType == 1 || $userType == 2) {
		header("Location: redirect_page.php");
	}

	$association_id = $_POST['id'];
	$moderator_id = $_POST['moderator_id'];
	$association_name = $_POST['name'];
	$association_content = $_POST['content'];

	$sql = "UPDATE udruga SET moderator_id='$moderator_id', naziv='$association_name', opis='$association_content' WHERE udruga_id='$association_id' ";
	$result = executeQuery($sql);

	if ($result == true) {
		echo "Udruga je ažurirana";
	} else {
		echo "Dogodila se pogreška";
	} 
?>			
