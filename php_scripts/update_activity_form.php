<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");
		include_once ("functions.php");
		include_once ("database.php");
		include_once ("current_user_functions.php");
		connectOnDatabase();

		$userId = getLoggedUserId();

		if (empty($userId) || $userType == 2) {
		header("Location: redirect_page.php");
	}

		$activity_id = $_GET['aktivnost_id'];

		$sql = "SELECT * FROM korisnik";
		$queryUsers = executeQuery($sql);

		$sql = "SELECT * FROM aktivnost WHERE aktivnost_id = '$activity_id'";
		$result = executeQuery($sql);
		$activity = mysql_fetch_array($result);

		$sqlQuery = "SELECT korisnik_id FROM sudionik WHERE aktivnost_id='$activity_id'";
		$participents_id_query = executeQuery($sqlQuery);

		$participents_ids = [];

		while($row = mysql_fetch_array($participents_id_query)) {
			$participents_id = $row['korisnik_id'];
			array_push($participents_ids, $participents_id);
		}
	?>
	<div class="form">
		<form action="update_activity.php" method="POST" accept-charset="utf-8">
			<p>
				<input type="hidden" name="id" value="<?= $activity['aktivnost_id'] ?>" placeholder=""></input>
			</p>
			<p>
				<label>Naziv aktivnosti:</label>
				<input type="name" name="name" value="<?= $activity['naziv'] ?>" placeholder=""></input>
			</p>
			<p>
				<label>Opis aktivnosti:</label>
				<textarea rows="5" cols="50" name="content"><?= $activity['opis'] ?></textarea>
			</p>
			<p>
				<label>Datum održavanja aktivnosti(upiši format: dd.mm.gggg):</label>
				<input type="text" name="date" value="<?= getHumanReadableDate($activity['datum_odrzavanja']);?>" placeholder="" required=""></input>		
			</p>
			<p>
				<label>Vrijeme održavanja aktivnosti(upiši format: hh:mm:ss):</label>
				<input type="text" step="1" name="time" value="<?= $activity['vrijeme_odrzavanja'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<label>Sudionici: (za odabir više sudionika drži tipku CRTL)</label>
				<select name="participants[]" multiple size="20" class="select">
					<?php while ($user = mysql_fetch_array($queryUsers)) { ?>
						<option value="<?= $user['korisnik_id'] ?>" <?php if(in_array($user['korisnik_id'], $participents_ids)) {echo 'selected';} ?>
						>
							<?= $user['korisnicko_ime'] ?>
						</option>
					<?php } ?>
				</select>
			</p>

			<p>
				<input type="submit" name="update_activity" class="btn" value="Spremi promjene aktivnosti"></input>
			</p>
		</form>
	</div>
</body>
</html>

