<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");
		include_once ("current_user_functions.php");

		$userId = getLoggedUserId();
		$userType = getUserType();
		
		if (empty($userId) || $userType == 1 || $userType == 2) {
			header("Location: redirect_page.php");
		}
	?>

	<div class="form">
		<form action="created_association.php" method="POST" accept-charset="utf-8">
				<p>
				<label>Moderator id:</label>
				<select name="moderator_id" class="select">
					<?php
						$sql = "SELECT * FROM korisnik WHERE tip_id=1";
						$queryModerator = executeQuery($sql);
						 while ($moderator = mysql_fetch_array($queryModerator)) {
					?>
						<option value="<?= $moderator['korisnik_id'] ?>"><?= $moderator['korisnicko_ime'] ?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				<label>Naziv udruge:</label>
				<input type="name" name="a_name" value="" placeholder="Upiši naziv udruge" required=""></input>
			</p>
			<p>
				<label>Opis udruge:</label>
				<textarea rows="5" cols="50" placeholder="Upiši opis udruge" name="a_content" required=""></textarea>
			</p>
			<p>
				<input type="submit" name="created_association" class="btn" value="Kreiraj novu udrugu"></input>
			</p>
		</form>
	</div>
</body>
</html>