<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
	<?php 
		include_once ("navigation.php");
		include_once ("functions.php");
		include_once ("database.php");
		include_once ("current_user_functions.php");
		connectOnDatabase();

		$userType = getUserType();
		$userId = getLoggedUserId();

		if (empty($userId) || $userType == 1 || $userType == 2) {
			header("Location: redirect_page.php");
		}

		$user_id = $_GET['korisnik_id'];

		$sql = "SELECT * FROM korisnik WHERE korisnik_id = '$user_id'";
		$result = executeQuery($sql);
		$user = mysql_fetch_array($result);
		
	?>
	<div class="form">
		<form action="update_user.php" method="POST" accept-charset="utf-8">
			<?php
				$sql = "SELECT * FROM tip_korisnika";
				$queryUserType = executeQuery($sql);
			?>
			<p>
				<label class="tip">Tip korisnika:</label>
				<select name="userType" class="type_select">
					<?php while ($userType = mysql_fetch_array($queryUserType)) { ?>
						<option value="<?= $userType['tip_id'] ?>" 

						<?php
							if ($userType['tip_id'] == $user['tip_id']) {
								echo "selected";
							}
						?>
						> <?= $userType['naziv'] ?> </option>
					<?php } ?>
				</select>
			</p>	
			<p>
				<input type="hidden" name="id" value="<?= $user['korisnik_id'] ?>" placeholder=""></input>
			</p>
			<p>
				<label>Ime:</label>
				<input type="name" name="name" value="<?= $user['ime'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<label>Prezime:</label>
				<input type="surname" name="surname" value="<?= $user['prezime'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<label>Email:</label>
				<input type="email" name="email" value="<?= $user['email'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<label>Korisničko ime:</label>
				<input type="text" name="username" value="<?= $user['korisnicko_ime'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<label>Lozinka:</label>
				<input type="password" name="password" value="<?= $user['lozinka'] ?>" placeholder="" required=""></input>
			</p>
			<p>
				<input type="submit" name="update_user" class="btn" value="Spremi promjene korisnika"></input>
			</p>
		</form>
	</div>
</body>
</html>

