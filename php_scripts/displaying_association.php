<!DOCTYPE>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");	

			connectOnDatabase();

			$userType = getUserType();
			$userId = getLoggedUserId();

			if (!empty($userId) && $userType == 0) {
		?>
			<div>
				<a href="./created_association_form.php">Kreiraj novu udrugu</a>
			</div><br>
		<?php	} ?>

		<?php
			if (!empty($userId) && $userType == 0) {
		?>
			<div class="filter-container">
				<form action="displaying_association.php" method="POST" accept-charset="utf-8">
					<p>
						<label>Naziv udruge:</label>
						<input type="text" name="subject" placeholder="Upiši broj akrivnosti" ></input>
					</p>
					<p>
						<label>Sortiraj po:</label>
						<select name="sortUser">
							<option value="activityNumber">Broju aktivnosti</option>
							<option value="associationSubject">Nazivu udruge</option>
						</select>
					</p>
					<p>
						<input type="submit" name="create_activity" class="btn" value="Primjeni"></input>
					</p>
				</form>
			</div>
		<?php } ?>


		<?php

			$subject = '';
			if (isset($_POST['subject'])) {
				$subject = $_POST['subject'];
			}

			$sortBy='';
			if (isset($_POST['sortUser'])) {
				$sort = $_POST['sortUser'];

				if ($sort == "activityNumber") {
					$sortBy = "ORDER BY brojAktivnosti DESC";
				} else if ($sort == "associationSubject") {
					$sortBy = "ORDER BY u.naziv";
				}
			}

			$sql = "SELECT u.udruga_id, u.naziv, COUNT(a.aktivnost_id) as brojAktivnosti FROM udruga u LEFT JOIN aktivnost a ON u.udruga_id = a.udruga_id WHERE u.naziv LIKE '%$subject%' GROUP BY u.udruga_id $sortBy";

			
			$query_association = executeQuery($sql); 
		
			
		?>
			<table class="tbl">
			<tr>
				<th>Popis udruga</th>
				<?php
					if (!empty($userId) && $userType==0) {
				?>
						<th>Broj aktivnosti</th>
				<?php } ?>
			</tr>
			<tr>
				<?php while($row = mysql_fetch_array($query_association)) { ?>
				<td><a href="association_details.php?udruga_id=<?= $row['udruga_id']?>" ><?= $row['naziv']?></a></td>
				<?php
					if (!empty($userId) && $userType==0) {
				?>
					<td><?= $row['brojAktivnosti']?></td>
				<?php } ?>
			</tr>
			<?php }	?>
		</table>
		
	</body>
</html>

