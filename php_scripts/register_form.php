<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
	<?php
		include_once ("navigation.php");
		include_once ("current_user_functions.php");

		$userId = getLoggedUserId();
		if (!empty($userId)) {
			header("Location: redirect_page.php");
		}
	?>
	<div class="form">
		<form action="register.php" method="POST" accept-charset="utf-8">
			<p>
				<label>Ime:</label>
				<input type="name" name="name" value="" placeholder="Upiši ime" required=""></input>
			</p>
			<p>
				<label>Prezime:</label>
				<input type="surname" name="surname" value="" placeholder="Upiši prezime" required=""></input>
			</p>
			<p>
				<label>Email:</label>
				<input type="email" name="email" value="" placeholder="Upiši email" required=""></input>
			</p>
			<p>
				<label>Korisničko ime:</label>
				<input type="text" name="username" value="" placeholder="Upiši korisničko ime" required=""></input>
			</p>
			<p>
				<label>Lozinka:</label>
				<input type="password" name="password" value="" placeholder="Upiši lozinku" required=""></input>
			</p>
			<p>
				<input type="submit" name="register" class="btn" value="Registriraj se" required=""></input>
				
			</p>
		</form>
	</div>
	
</body>
</html>