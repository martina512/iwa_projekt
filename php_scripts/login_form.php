<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
</head>
<body>
	<?php
		include_once ("navigation.php");
		include_once ("current_user_functions.php");

		$error = "";
		if(isset($_GET['error'])) {
			$error = $_GET['error'];
		}

		$userId = getLoggedUserId();
		if (!empty($userId)) {
			header("Location: redirect_page.php");
		}
	?>
	<div class="form">
		<div><?= $error ?></div>

		<form action="login.php" method="POST" accept-charset="utf-8">
			<p>
				<label>Korisničko ime:</label>
				<input type="text" name="username" value="" placeholder="Upiši korisničko ime"></input>
			</p>
			<p>
				<label>Lozinka:</label>
				<input type="password" name="password" value="" placeholder="Upiši lozinku"></input>
			</p>
			<p>
				<input type="submit" name="login" class="btn" value="Prijavi se"></input>
			</p>
		</form>
	</div>
	
</body>
</html>