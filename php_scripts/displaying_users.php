<!DOCTYPE>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/design.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			connectOnDatabase();
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");

			$userType = getUserType();
			$userId = getLoggedUserId();
			
			if (empty($userId) || $userType == 1 || $userType == 2) {
				header("Location: redirect_page.php");
			}
		?>
		<div>
			<a href="./created_user_form.php">Kreiraj novog korisnika</a>
		</div><br>


		<div class="filter-container">
				<form action="displaying_users.php" method="POST" accept-charset="utf-8">
					<p>
						<label>Ime ili prezime korisnika:</label>
						<input type="text" name="name" value="" placeholder="Upiši ime korisnika" ></input>		
					</p>
					<p>
						<label>Tip korisnika:</label>
				<?php
						include_once ("database.php");
						include_once ("functions.php");
						include_once ("current_user_functions.php");

						connectOnDatabase();

						$sql = "SELECT * FROM tip_korisnika";
						$query = executeQuery($sql);
				?>
						<select name="userType">
							<option value="-1"></option>
							<?php while ($row = mysql_fetch_array($query)) { ?>
								<option value="<?= $row['tip_id'] ?>"><?= $row['naziv'] ?></option>
							<?php } ?>	
						</select>
					</p>
					<p>
						<label>Sortiraj po:</label>
						<select name="sortUser">
							<option value="activityNumber">Broju aktivnosti</option>
							<option value="userName">Nazivu (prezime) korisnika</option>
						</select>
					</p>
					<p>
						<input type="submit" name="create_activity" class="btn" value="Primjeni"></input>
					</p>
				</form>
			</div>


		<?php

			$name = '';
			if (isset($_POST['name'])) {
				$name = $_POST['name'];
			}

			$userType = '';
			$type='';
			if (isset($_POST['userType']) && $_POST['userType'] != -1) {
				$userType = $_POST['userType'];
				$type = "AND k.tip_id=".$userType;
			}

			$sortBy='';
			if (isset($_POST['sortUser'])) {
				$sort = $_POST['sortUser'];

				if ($sort == "activityNumber") {
					$sortBy = "ORDER BY brojAktivnosti DESC";
				} else if ($sort == "userName") {
					$sortBy = "ORDER BY k.prezime";
				}
			}


			$sql = "SELECT k.korisnicko_ime, k.ime, k.prezime, k.korisnik_id, count(s.aktivnost_id) as brojAktivnosti FROM korisnik k LEFT JOIN sudionik s ON k.korisnik_id = s.korisnik_id WHERE (k.ime LIKE '%$name%' OR k.prezime LIKE '%$name%') $type GROUP BY k.korisnik_id $sortBy";


			$query_user = executeQuery($sql);
			
		?>
			<table class="tbl">
			<tr>
				<th>Korisničko ime korisnika</th>
				<th>Ime korisnika</th>
				<th>Prezime korisnika</th>
				<th>Broj aktivnosti</th>
			</tr>
			<tr>
				<?php while($users = mysql_fetch_array($query_user)) { ?>
				<td><a href="user_details.php?korisnik_id=<?= $users['korisnik_id']?>"> <?= $users['korisnicko_ime']?> </a></td>
				<td><?= $users['ime']?></td>
				<td><?= $users['prezime']?></td>
				<td><?= $users['brojAktivnosti']?></td>
			</tr>
		<?php }	?>
		</table>


		