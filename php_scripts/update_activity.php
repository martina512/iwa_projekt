<?php
	include_once ("database.php");
	include_once ("functions.php");
	include_once ("navigation.php");
	include_once ("current_user_functions.php");
	
	connectOnDatabase();

	$userId = getLoggedUserId();

	if (empty($userId)) {
		header("Location: redirect_page.php");
	}

	$activity_name = $_POST['name'];
	$activity_content = $_POST['content'];
	$activity_id = $_POST['id'];
	$date = getFormattedDate($_POST['date']);
	$time = $_POST['time'];
	
	$sudionici = [];
	if (!empty($_POST["participants"])) {
		$sudionici = $_POST["participants"];
	}

	$sql = "UPDATE aktivnost SET naziv='$activity_name', opis='$activity_content', datum_odrzavanja='$date', vrijeme_odrzavanja='$time' WHERE aktivnost_id='$activity_id' ";
	$result = executeQuery($sql);
	
	if ($result == true) {	
		$sqlQuery = "DELETE FROM sudionik WHERE aktivnost_id='$activity_id'";
		executeQuery($sqlQuery);

		for ($i=0; $i<count($sudionici); $i++) {
			$sql = "INSERT INTO sudionik (aktivnost_id, korisnik_id) VALUES ('$activity_id', '$sudionici[$i]')";
			executeQuery($sql);
		}
		echo "Aktivnost je ažurirana";

	} else {
		echo "Dogodila se pogreška";
	} 
?>			

