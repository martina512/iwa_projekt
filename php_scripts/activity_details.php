<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/common.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");
			connectOnDatabase();

			$userId = getLoggedUserId();
			$userType = getUserType();

			if (empty($userId)) {
				header("Location: redirect_page.php");
			} 
			
			$aktivnost_id = $_GET['aktivnost_id'];

			$sql = "SELECT * FROM aktivnost WHERE aktivnost_id ='$aktivnost_id'";
			$query_activity = executeQuery($sql); 

			$activity = mysql_fetch_array($query_activity);

		?>	
		<div>
			<a href="./user_activities.php?aktivnost_id=<?= $activity['aktivnost_id']?>">Popis svih sudionika na aktivnosti</a>
		</div>
		
		<div>
			<table class="tbl_association">
				<p>Detalji aktivnosti</p>
				<tr>
					<th>Naziv aktivnosti:</th>
					<td><?= $activity["naziv"] ?></td>
				</tr>
				<tr>
					<th>Opis aktivnosti:</th>
					<td><?= $activity["opis"] ?></td>
				</tr>
				<tr>
					<th>Datum kreiranja aktivnosti:</th>
					<td><?= getHumanReadableDate($activity["datum_kreiranja"]); ?></td>
				</tr>
				<tr>
					<th>Vrijeme kreiranja aktivnosti:</th>
					<td><?= $activity["vrijeme_kreiranja"] ?></td>
				</tr>
				<tr>
					<th>Datum održavanja aktivnosti:</th>
					<td><?= getHumanReadableDate($activity["datum_odrzavanja"]); ?></td>
				</tr>
				<tr>
					<th>Vrijeme održavanja aktivnosti:</th>
					<td><?= $activity["vrijeme_odrzavanja"] ?></td>
				</tr>
				<tr>
					<?php if ($userType==1 || $userType==0) { ?>
					<th>Ažuriraj aktivnost:</th>
					<td><a href="./update_activity_form.php?aktivnost_id=<?= $activity['aktivnost_id'] ?>">Ažuriraj aktivnost</a></td>
					<?php } ?>
				</tr>
			</table>
		</div>