<!DOCTYPE>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../stylesheet/style.css">
		<link rel="stylesheet" type="text/css" href="../stylesheet/.css">
	</head>

	<body>
		<?php
			include_once ("database.php");
			include_once ("functions.php");
			include_once ("navigation.php");
			include_once ("current_user_functions.php");

			connectOnDatabase();

			$userId = getLoggedUserId();
			$userType = getUserType();

			if (empty($userId)) {
				header("Location: redirect_page.php");
			} 
		?>

			<div class="filter-container">
				<form action="activity.php" method="POST" accept-charset="utf-8">
					<p>
						<label>Naziv udruge:</label>
						<input type="text" name="subject" value="" placeholder="Upiši naziv udruge" ></input>		
					</p>
					<p>
						<label>Datum OD:(upiši format: dd.mm.gggg. hh:mm:ss)</label>
						<input type="text" name="dateFrom" value="" placeholder="DD.MM.GGGG. HH:MM:SS" ></input>
					</p>
					<p>
						<label>Datum DO:(upiši format: dd.mm.gggg. hh:mm:ss)</label>
						<input type="text" name="dateTo" value="" placeholder="DD.MM.GGGG. HH:MM:SS" ></input>
					</p>
					<p>
						<input type="submit" name="create_activity" class="btn" value="Filtriraj"></input>
					</p>
				</form>
			</div>

		<?php
			$naziv = '';
			if (isset($_POST['subject'])) {
				$naziv = $_POST['subject'];
			}

			$dateFrom = "";
			if (isset($_POST['dateFrom']) && !empty($_POST['dateFrom'])) {
				$date_from = $_POST['dateFrom'];
				$formattedDate = getDateFromDateTime($date_from);
				$formattedTime = getTimeFromDateTime($date_from);
				$dateFrom = "AND (datum_odrzavanja > '$formattedDate' OR (datum_odrzavanja = '$formattedDate' AND vrijeme_odrzavanja >= '$formattedTime'))";
			}

			$dateTo = "";
			if (isset($_POST['dateTo']) && !empty($_POST['dateTo'])) {
				$date_to = $_POST['dateTo'];
				$formattedDate = getDateFromDateTime($date_to);
				$formattedTime = getTimeFromDateTime($date_to);
				$dateTo = "AND (datum_odrzavanja < '$formattedDate' OR (datum_odrzavanja = '$formattedDate' AND vrijeme_odrzavanja <= '$formattedTime'))";
			}

			$sql = '';
			if ($userType==2) {
				$sql = "SELECT DISTINCT a.aktivnost_id, a.naziv as nazivAktivnosti, u.naziv as nazivUdruge, a.datum_odrzavanja FROM aktivnost a, sudionik s, udruga u WHERE s.aktivnost_id=a.aktivnost_id AND s.korisnik_id = '$userId' AND u.udruga_id=a.udruga_id AND u.naziv LIKE '%$naziv%' $dateFrom $dateTo ORDER BY a.naziv";
			} else if ($userType==1) {
				$sql = "SELECT DISTINCT a.aktivnost_id, a.naziv as nazivAktivnosti, u.naziv as nazivUdruge, a.datum_odrzavanja FROM aktivnost a, udruga u WHERE u.udruga_id = a.udruga_id AND u.moderator_id = $userId AND u.naziv LIKE '%$naziv%' $dateFrom $dateTo ORDER BY a.naziv";
			} else if ($userType==0) {
				$sql ="SELECT DISTINCT a.aktivnost_id, a.naziv as nazivAktivnosti, u.naziv as nazivUdruge, a.datum_odrzavanja FROM aktivnost a, udruga u WHERE u.udruga_id = a.udruga_id AND u.naziv LIKE '%$naziv%' $dateFrom $dateTo ORDER BY a.naziv";
			} else {
				header("Location: redirect_page.php");
			}


			$query_activity = executeQuery($sql); 
		?>

		<table class="tbl">
			<tr>
				<th>Naziv aktivnosti</th>
				<th>Naziv udruge</th>
				<th>Popis svih sudionika</th>
			</tr>
			<tr>
				<?php while($row = mysql_fetch_array($query_activity)) { ?>
				<td><a href="activity_details.php?aktivnost_id=<?= $row['aktivnost_id']?>"> <?=$row['nazivAktivnosti']?> </a></td>
				<td><?= $row['nazivUdruge']?></td>
				<td>
					<a href="./user_activities.php?aktivnost_id=<?= $row['aktivnost_id']?>">Popis svih sudionika aktivnosti</a>
				</td>
			</tr>
			<?php }	?>
		</table>
	
	</body>
</html>

				